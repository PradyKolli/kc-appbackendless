//
//  School.swift
//  The Most Awesome Northwest-KC Programming Contest
//
//  Created by Pradeep Kolli on 3/14/19.
//  Copyright © 2019 northwest. All rights reserved.
//

import Foundation
@objcMembers
class School: NSObject{
    static func == (lhs: School, rhs: School) -> Bool {
        if lhs.name == rhs.name && lhs.coach == rhs.coach{
            return true
        }
        else{
            return false
        }
    }
    
    var name:String?
    var coach:String?
    var teams:[Team] = []
    let backendless:Backendless = Backendless.sharedInstance()! // our singleton
    var schoolDataStore:IDataStore!
    var objectId:String?
    private override init() {
        schoolDataStore = backendless.data.of(School.self)
    }
    init(name:String, coach:String, teams:[Team]) {
        self.name = name
        self.coach = coach
        self.teams = teams
    }
//    func addTeam(name:String, students:[String]){
//        self.teams.append(Team(name:name,students:students))
//    }
    
}
