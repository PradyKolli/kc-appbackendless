//
//  AddSchoolViewController.swift
//  The Most Awesome Northwest-KC Programming Contest
//
//  Created by Pradeep Kolli on 3/14/19.
//  Copyright © 2019 northwest. All rights reserved.
//

import UIKit

class AddSchoolViewController: UIViewController {

    @IBOutlet weak var nameTF: UITextField!
    @IBOutlet weak var coachTF: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    

    @IBAction func doneActionBTN(_ sender: Any) {
        if nameTF.text! == "" || coachTF.text! == ""{
            displayMessage()
        }
        else{
            let sch1 = School(name: nameTF.text!, coach: coachTF.text!, teams: [])
            Schools.shared.add(school:sch1)
            self.dismiss(animated: true, completion: nil)
        }
        
    }
    
    @IBAction func cancelActionBTN(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    func displayMessage(){
        let alert = UIAlertController(title: "Warning",
                                      message: "Please give valid values",
                                    preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
