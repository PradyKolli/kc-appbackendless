//
//  DataModels.swift
//  The Most Awesome Northwest-KC Programming Contest
//
//  Created by Pradeep Kolli on 3/14/19.
//  Copyright © 2019 northwest. All rights reserved.
//
extension Notification.Name {
    static let SchoolsRetrieved = Notification.Name("all schools retrieved")
    static let TeamsForSelectedCityRetrieved = Notification.Name("teams for the selected school retrieved")
    static let TeamsRetrieved = Notification.Name("all teams retrieved")
}

import Foundation
@objcMembers
class Schools:NSObject{
    let backendless = Backendless.sharedInstance()
    var schoolDataStore:IDataStore!
    var teamsDataStore:IDataStore!
    static let shared = Schools()
    private var schools:[School]
    var objectid:String?
    var teams:[Team] = []
    var selectedSchool:School?
    var teamsForSelectedSchool:[Team] = []
    private convenience override init(){
        self.init(schools:[])
        schoolDataStore = backendless!.data.of(School.self)
        teamsDataStore = backendless!.data.of(Team.self)
    }
    private init(schools:[School]){
        self.schools = schools
    }
    func numSchools() -> Int{
        return schools.count
    }
    subscript(index:Int) -> School {
        return schools[index]
    }
    func add(school:School){
        schoolDataStore.save(school)
        schools.append(school)
    }
    func addTeam(_ team:Team, toSchool school:School){
        let teamItemBe = teamsDataStore.save(team) as! Team
        self.schoolDataStore.addRelation("teams:Team:n", parentObjectId:school.objectId, childObjects: [teamItemBe.objectId!])
        school.teams.append(teamItemBe)
    }
//    func delete(school:School){
//        for sch in 0..<schools.count{
//            if schools[sch] == school{
//                schools.remove(at:sch)
//            }
//        }
//    }
    
    func delete(school:School){
        for i in 0..<schools.count{
            if schools[i] == school{
                schools.remove(at: i)
                let query:DataQueryBuilder = DataQueryBuilder()
                query.setRelated(["teams"])
                let arrayOfSchools = schoolDataStore.find(query) as! [School]
                let arrayOfTeams = arrayOfSchools[i].teams
                for i in 0..<arrayOfTeams.count{
                    self.teamsDataStore.remove(byId: arrayOfTeams[i].objectId)
                }
                self.schoolDataStore.remove(byId: school.objectId)
                return
            }
        }
    }
    func retreiveAllSchools(){
        let queryBuilder = DataQueryBuilder()
        queryBuilder!.setRelated(["teams"])
        queryBuilder!.setPageSize(100)
        Types.tryblock({() -> Void in
            self.schools = self.schoolDataStore.find() as! [School]
            
        },
                       catchblock: {(fault) -> Void in print(fault ?? "Something has gone wrong while retrieving all the schools")}
        )
    }
}
