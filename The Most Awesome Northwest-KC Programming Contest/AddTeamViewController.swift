//
//  AddTeamViewController.swift
//  The Most Awesome Northwest-KC Programming Contest
//
//  Created by Pradeep Kolli on 3/14/19.
//  Copyright © 2019 northwest. All rights reserved.
//

import UIKit

class AddTeamViewController: UIViewController {
    var school:School!
    @IBOutlet weak var thirdStudentTF: UITextField!
    @IBOutlet weak var secondStudentTF: UITextField!
    @IBOutlet weak var firstStudentTF: UITextField!
    @IBOutlet weak var teamNameTF: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    @IBAction func doneActionBTN(_ sender: Any) {
        if teamNameTF.text! == "" || firstStudentTF.text! == "" || secondStudentTF.text! == "" || thirdStudentTF.text == ""{
            displayMessage()
        }
        else{
            let nameOfTeam = teamNameTF.text!
            let s1 = firstStudentTF.text!
            let s2 = secondStudentTF.text!
            let s3 = thirdStudentTF.text!
            let teamToBeAdded = Team(name: nameOfTeam, student01: s1, student02: s2, student03: s3)
//            school.addTeam(name: nameOfTeam, students: studentslist)
            Schools.shared.addTeam(teamToBeAdded, toSchool: school)
            self.dismiss(animated: true, completion: nil)
            
        }
    }
    
    @IBAction func cancelActionBTN(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    func displayMessage(){
        let alert = UIAlertController(title: "Warning",
                                      message: "Please enter valid values",
                                      preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        self.present(alert, animated: true, completion: nil)
        
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
