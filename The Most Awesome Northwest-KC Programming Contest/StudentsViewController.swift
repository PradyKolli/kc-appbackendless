//
//  StudentsViewController.swift
//  The Most Awesome Northwest-KC Programming Contest
//
//  Created by Pradeep Kolli on 3/14/19.
//  Copyright © 2019 northwest. All rights reserved.
//

import UIKit

class StudentsViewController: UIViewController {
    var team:Team!
    override func viewDidLoad() {
        super.viewDidLoad()
        firstStudentLBL.text = team.student00
        secondStudentLBL.text = team.student01
        thirdStudentLBL.text = team.student02
        navigationItem.title = team.name
        // Do any additional setup after loading the view.
    }
    
    @IBOutlet weak var firstStudentLBL: UILabel!
    @IBOutlet weak var thirdStudentLBL: UILabel!
    @IBOutlet weak var secondStudentLBL: UILabel!
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
