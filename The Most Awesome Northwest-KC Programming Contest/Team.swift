//
//  Team.swift
//  The Most Awesome Northwest-KC Programming Contest
//
//  Created by Pradeep Kolli on 3/14/19.
//  Copyright © 2019 northwest. All rights reserved.
//

import Foundation
@objcMembers
class Team: NSObject{
    var name:String?
//    var students:[String] = []
    var student00:String?
    var student01:String?
    var student02:String?
    let backendless:Backendless = Backendless.sharedInstance()! // our singleton
    var teamsDataStore:IDataStore!
    var objectId:String?
    
    init(name:String?, student01:String?, student02:String?, student03:String?){
        self.name=name
        self.student00=student01
        self.student01=student02
        self.student02=student03
    }
    
    private override init() {
        teamsDataStore = backendless.data.of(Team.self)
    }
}
